{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Training a model with MLflow tracking\n",
    "\n",
    "In this notebook, we're going to train a model on our local machine. We will also log training parameters and metrics thanks to the [MLflow](https://mlflow.org) integration built in the [transformers](https://github.com/huggingface/transformers) library.\n",
    "\n",
    "This integration relies on the [MLflowCallback](https://huggingface.co/docs/transformers/v4.20.1/en/main_classes/callback#transformers.integrations.MLflowCallback) object. This callback is enabled automatically if ```mlflow``` is installed in your Python environment. You don't need to change any code in your training script!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 1 - Setup"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's make sure ```mlflow``` is installed."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "%%sh\n",
    "pip -q install torch transformers datasets widgetsnbextension ipywidgets huggingface_hub mlflow --upgrade"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "\n",
    "import datasets\n",
    "import pandas as pd\n",
    "import transformers\n",
    "from matplotlib import pyplot\n",
    "\n",
    "print(transformers.__version__)\n",
    "print(datasets.__version__)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 2 - Train locally"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from datasets import load_dataset\n",
    "from sklearn.metrics import accuracy_score, precision_recall_fscore_support\n",
    "from transformers import (\n",
    "    AutoModelForSequenceClassification,\n",
    "    AutoTokenizer,\n",
    "    Trainer,\n",
    "    TrainingArguments,\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First, we define the name of base model we're going to start from, as well as some usual hyperparameters."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "base_model_id = \"distilbert-base-uncased\"\n",
    "\n",
    "epochs = 1\n",
    "num_labels = 5  # Dataset has 5 classes\n",
    "learning_rate = 5e-5\n",
    "train_batch_size = 32\n",
    "eval_batch_size = 64\n",
    "save_strategy = \"no\"\n",
    "save_steps = 500\n",
    "logging_steps = 100\n",
    "\n",
    "output_data_dir = \"./output\"\n",
    "model_dir = \"./model\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Then, we load the dataset we processed in the previous notebook. We can either load it from the Hugging Face Hub or from disk."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Option 1: load dataset from the Hub\n",
    "dataset_name = \"juliensimon/amazon-shoe-reviews\"\n",
    "dataset = load_dataset(dataset_name)\n",
    "\n",
    "# Option 2: load dataset from local storage\n",
    "# dataset = load_from_disk(\"./data\")\n",
    "\n",
    "print(dataset)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dataset[\"train\"][0]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "train_dataset = dataset[\"train\"]\n",
    "valid_dataset = dataset[\"test\"]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Then, we define a function that computes the different metrics that we'd like to log during training."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def compute_metrics(pred):\n",
    "    labels = pred.label_ids\n",
    "    preds = pred.predictions.argmax(-1)\n",
    "    precision, recall, f1, _ = precision_recall_fscore_support(labels, preds)\n",
    "    acc = accuracy_score(labels, preds)\n",
    "    return {\"accuracy\": acc, \"f1\": f1, \"precision\": precision, \"recall\": recall}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, we download the base model and its tokenizer from the Hugging Face Hub."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model = AutoModelForSequenceClassification.from_pretrained(base_model_id, num_labels=num_labels)\n",
    "tokenizer = AutoTokenizer.from_pretrained(base_model_id)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, we define a function to tokenize the datasets. We'll process them in batches to speed things up. We apply this function to both datasets with the ```map()``` function available in the ```datasets``` library."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def tokenize(batch):\n",
    "    return tokenizer(batch[\"text\"], padding=\"max_length\", truncation=True)\n",
    "\n",
    "\n",
    "train_dataset = train_dataset.map(tokenize, batched=True, batch_size=len(train_dataset))\n",
    "valid_dataset = valid_dataset.map(tokenize, batched=True, batch_size=len(valid_dataset))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We remove the ```text``` column as it's not needed anymore. This is an optional step."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "train_dataset = train_dataset.remove_columns([\"text\"])\n",
    "valid_dataset = valid_dataset.remove_columns([\"text\"])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We define the ```TrainingArguments``` for our training job: hyperparameters, where to save the model, etc. \n",
    "\n",
    "Documentation: https://huggingface.co/docs/transformers/main/en/main_classes/trainer#transformers.TrainingArguments"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "hub_model_id = \"juliensimon/distilbert-amazon-shoe-reviews-mlflow\"\n",
    "\n",
    "training_args = TrainingArguments(\n",
    "    hub_model_id=hub_model_id,  # This is where we'll push the model after training\n",
    "    output_dir=model_dir,\n",
    "    num_train_epochs=epochs,\n",
    "    per_device_train_batch_size=train_batch_size,\n",
    "    per_device_eval_batch_size=eval_batch_size,\n",
    "    save_strategy=save_strategy,\n",
    "    save_steps=save_steps,\n",
    "    evaluation_strategy=\"epoch\",\n",
    "    learning_rate=learning_rate,\n",
    "    logging_steps=logging_steps,\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Then, we use the ```Trainer``` object to put all the pieces together.\n",
    "\n",
    "Documentation: https://huggingface.co/docs/transformers/main/en/main_classes/trainer#transformers.Trainer"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "trainer = Trainer(\n",
    "    model=model,\n",
    "    args=training_args,\n",
    "    tokenizer=tokenizer,\n",
    "    compute_metrics=compute_metrics,\n",
    "    train_dataset=train_dataset,\n",
    "    eval_dataset=valid_dataset,\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We're now ready to train. Let's just give our ```mlflow``` experiment a name."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "os.environ[\"MLFLOW_EXPERIMENT_NAME\"] = \"trainer-mlflow-demo\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "trainer.train()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 3 - Pushing the model and the logs to the Hub"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's first push the model to the Hugging Face Hub."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "trainer.push_to_hub()\n",
    "\n",
    "# Another way to do this is to set push_to_hub=True in training_args"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The model is now visible at https://huggingface.co/juliensimon/distilbert-amazon-shoe-reviews-mlflow\n",
    "\n",
    "Now, we can add the MLflow logs to the model repository. They're stored in the ```mlruns``` directory.\n",
    "\n",
    "```\n",
    "cp -r mlruns model\n",
    "\n",
    "cd model\n",
    "\n",
    "git add mlruns\n",
    "\n",
    "git commit -m 'Add MLflow logs'\n",
    "\n",
    "git push\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 4 - Visualizing MLflow metrics"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The format of the ```mlflow``` logs is very simple. We can easily load them and plot them with ```matplotlib```."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!ls -lR mlruns"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "run = \"0/84d34e70933743b0a3f85712dee0a7cc\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def plot_metric(metric_name):\n",
    "    metric = pd.read_csv(\n",
    "        f\"mlruns/{run}/metrics/{metric_name}\", sep=\" \", names=[\"timestamp\", \"value\", \"steps\"]\n",
    "    )\n",
    "    pyplot.plot(metric[\"steps\"], metric[\"value\"])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plot_metric(\"loss\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pyplot.clf()\n",
    "plot_metric(\"learning_rate\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Of course, we can also visualize them in the ```mlflow``` user interface. Just run ```mlflow ui``` in the directory that contains ```mlruns``` :)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "instance_type": "ml.t3.medium",
  "kernelspec": {
   "display_name": "conda_pytorch_p38",
   "language": "python",
   "name": "conda_pytorch_p38"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
